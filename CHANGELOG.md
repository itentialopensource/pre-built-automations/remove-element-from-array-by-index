
## 0.2.9 [02-03-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-index!21

---

## 0.2.8 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-index!20

---

## 0.2.7 [06-14-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-index!19

---

## 0.2.6 [12-03-2021]

* Certified for 2021.2

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-index!18

---

## 0.2.5 [07-02-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-index!17

---

## 0.2.4 [06-11-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-index!16

---

## 0.2.3 [12-23-2020]

* README 2020.2 updates

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-index!15

---

## 0.2.2 [10-21-2020]

* Update package.json and README.md

See merge request itentialopensource/pre-built-automations/remove-element-from-array-by-index!14

---

## 0.2.1 [07-21-2020]

* Update package.json

See merge request itentialopensource/pre-built-automations/remove-element-from-array!13

---

## 0.2.0 [07-21-2020]

* Removed Array length

See merge request itentialopensource/pre-built-automations/remove-element-from-array!12

---

## 0.1.1 [07-08-2020]

* Update package.json to have consistent naming

See merge request itentialopensource/pre-built-automations/remove-element-from-array!8

---

## 0.1.0 [06-18-2020]

* [minor/LB-404] Add './' to path

See merge request itentialopensource/pre-built-automations/remove-element-from-array!6

---

## 0.0.2 [05-13-2020]

* Update images/transformations.png, README.md, bundles/transformations/Remove...

See merge request itentialopensource/pre-built-automations/Remove-Element-From-Array!5

---

## 0.0.2 [05-13-2020]

* Update images/transformations.png, README.md, bundles/transformations/Remove...

See merge request itentialopensource/pre-built-automations/Remove-Element-From-Array!5

---\n\n
